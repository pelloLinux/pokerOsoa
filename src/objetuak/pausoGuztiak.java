package objetuak;

import java.util.LinkedList;

public class pausoGuztiak {
	
	private LinkedList<pausoa> preFlop;
	private LinkedList<pausoa> flop;
	private LinkedList<pausoa> turn;
	private LinkedList<pausoa> river;
	
	
	public pausoGuztiak(LinkedList<pausoa> preFlop, LinkedList<pausoa> flop, LinkedList<pausoa> turn, LinkedList<pausoa> river){
		this.preFlop=preFlop;
		this.flop=flop;
		this.turn=turn;
		this.river=river;
	}
	
	public LinkedList<pausoa> getPreFlop() {
		return preFlop;
	}
	public void setPreFlop(LinkedList<pausoa> preFlop) {
		this.preFlop = preFlop;
	}
	public LinkedList<pausoa> getFlop() {
		return flop;
	}
	public void setFlop(LinkedList<pausoa> flop) {
		this.flop = flop;
	}
	public LinkedList<pausoa> getTurn() {
		return turn;
	}
	public void setTurn(LinkedList<pausoa> turn) {
		this.turn = turn;
	}
	public LinkedList<pausoa> getRiver() {
		return river;
	}
	public void setRiver(LinkedList<pausoa> river) {
		this.river = river;
	}

}
