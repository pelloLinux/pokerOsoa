package objetuak;

import java.util.LinkedList;

public class player {

	private boolean ezaguna;
	private String posizioa;
	private float diruKop;
	private boolean irabazlea;
	private LinkedList<karta> kartak;
	
	public player(boolean ezaguna, String posizioa, float diruKop)
	{
		this.setEzaguna(ezaguna);
		this.posizioa = posizioa;
		this.diruKop = diruKop;
	}
	public player(boolean ezaguna, String posizioa, float diruKop, LinkedList<karta> kartak)
	{
		this.setEzaguna(ezaguna);
		this.posizioa = posizioa;
		this.diruKop = diruKop;
		if(this.ezaguna)this.setKartak(kartak);
	}
	
	public float getDiruKop() {
		return diruKop;
	}
	public void setDiruKop(float diruKop) {
		this.diruKop = diruKop;
	}
	public String getPosizioa() {
		return posizioa;
	}
	public void setPosizioa(String posizioa) {
		this.posizioa = posizioa;
	}
	public boolean isEzaguna() {
		return ezaguna;
	}

	public void setEzaguna(boolean ezaguna) {
		this.ezaguna = ezaguna;
	}
	public LinkedList<karta> getKartak() {
		if(this.ezaguna)return kartak;
		else return null;
	}
	public void setKartak(LinkedList<karta> kartak) {
		if(this.ezaguna || this.irabazlea)this.kartak = kartak;
	}
	public boolean isIrabazlea() {
		return irabazlea;
	}
	public void setIrabazlea(boolean irabazlea) {
		this.irabazlea = irabazlea;
	}
}
