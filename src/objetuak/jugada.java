package objetuak;

import java.util.LinkedList;

public class jugada {
	
	private String zer;
	private String zeNumero;
	private LinkedList<karta> zeinKarta;
	private int nireZenbat;
	
	public jugada(){
		
		this.zer = "";
		this.zeNumero ="";
		this.setZeinKarta(new LinkedList<karta>());	
	}
	
	public jugada(String zer, String zeNumero, LinkedList<karta> l, int nireZenbat){
		
		this.zer = zer;
		this.zeNumero = zeNumero;
		this.setZeinKarta(l);	
		this.nireZenbat = nireZenbat;
	}
		
	public String getZer() {
		return zer;
	}
	public void setZer(String zer) {
		this.zer = zer;
	}

	public LinkedList<karta> getZeinKarta() {
		return zeinKarta;
	}

	public void setZeinKarta(LinkedList<karta> zeinKarta) {
		this.zeinKarta = zeinKarta;
	}

	public String getZeNumero() {
		return zeNumero;
	}

	public void setZeNumero(String zeNumero) {
		this.zeNumero = zeNumero;
	}


}
