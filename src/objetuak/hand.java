package objetuak;

import java.util.LinkedList;

public class hand {

	private int id;
	private boolean torneoa;
	private int jokalariKop;
	private float ciegaTxikia;
	private float ciegaHandia;
	private LinkedList<karta> erdikoKartak;
	private LinkedList<player> players;
	private pausoGuztiak pausoka;
	private boolean ezagunakIrabazi;
	private String inforDena;
	
	public hand(int id, boolean torneoa, float ciegaTxikia, float ciegaHandia, int jokalariKop,LinkedList<karta> erdikoKartak, LinkedList<player> players, pausoGuztiak pausoka, boolean irabazi)
	{
		this.id = id;
		this.torneoa = torneoa;
		this.ciegaTxikia = ciegaTxikia;
		this.ciegaHandia = ciegaHandia;
		this.jokalariKop = jokalariKop;
		this.erdikoKartak = erdikoKartak;
		this.players = players;
		this.setPausoka(pausoka);
		this.ezagunakIrabazi=irabazi;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LinkedList<karta> getErdikoKartak() {
		return erdikoKartak;
	}

	public void setErdikoKartak(LinkedList<karta> erdikoKartak) {
		this.erdikoKartak = erdikoKartak;
	}

	public LinkedList<player> getPlayers() {
		return players;
	}

	public void setPlayers(LinkedList<player> players) {
		this.players = players;
	}

	public int getJokalariKop() {
		return jokalariKop;
	}

	public void setJokalariKop(int jokalariKop) {
		this.jokalariKop = jokalariKop;
	}

	public float getCiegaTxikia() {
		return ciegaTxikia;
	}

	public void setCiegaTxikia(float ciegaTxikia) {
		this.ciegaTxikia = ciegaTxikia;
	}

	public boolean isTorneoa() {
		return torneoa;
	}

	public void setTorneoa(boolean torneoa) {
		this.torneoa = torneoa;
	}

	public float getCiegaHandia() {
		return ciegaHandia;
	}

	public void setCiegaHandia(float ciegaHandia) {
		this.ciegaHandia = ciegaHandia;
	}

	public pausoGuztiak getPausoka() {
		return pausoka;
	}

	public void setPausoka(pausoGuztiak pausoka) {
		this.pausoka = pausoka;
	}
	public void jokalariakInprimatu(){
		for(int i=0; i<this.getPlayers().size(); i++)System.out.println(i+". "+this.getPlayers().get(i));
	}

	public boolean isEzagunakIrabazi() {
		return ezagunakIrabazi;
	}

	public void setEzagunakIrabazi(boolean ezagunakIrabazi) {
		this.ezagunakIrabazi = ezagunakIrabazi;
	}
	
	public player ezagunaLortu(){
	
		for (int i = 0; i < this.players.size(); i++) {
			if(this.players.get(i).isEzaguna())return this.players.get(i);
		}
		return null;
	}

	public String getInforDena() {
		return inforDena;
	}

	public void setinforDena(String inforDena) {
		this.inforDena = inforDena;
	}
}
