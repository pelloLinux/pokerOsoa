package objetuak;

public class pausoa {

	private player zeinek;
	private String zer;
	private int zenbat;
	
	public pausoa(player zeinek, String zer, int zenbat)
	{
		this.zeinek = zeinek;
		this.zer = zer; //check/fold/raise/call
		this.zenbat = zenbat;//zenbat bb
	}

	public player getZeinek() {
		return zeinek;
	}
	public void setZeinek(player zeinek) {
		this.zeinek = zeinek;
	}
	public String getZer() {
		return zer;
	}
	public void setZer(String zer) {
		this.zer = zer;
	}
	public int getZenbat() {
		return zenbat;
	}
	public void setZenbat(int zenbat) {
		this.zenbat = zenbat;
	}
}
