package pokerOsoa;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;
import javax.imageio.ImageIO;

import objetuak.karta;
import objetuak.pausoa;
import objetuak.player;

public class funtzioak extends Thread {

	static int time1 = 500;
	static int time2 = 1000;
	static int time3 = 1500;

	static LinkedList<pausoa> zerGertatuDa = new LinkedList<pausoa>();//zer gertatu da desberdinak egin behar dira. Turnerako, river..
	static String zeMomentutan = "";
	static int ciegaHandia = -1;
	static int aurrekoCiega = -1;
	static boolean ciegaHartu = true;
	static int ante = 2;//kanpeonatoaren arabera aldatzen da
	static boolean raiseEginda = false;
	static int raiseZenbat = -1;
	static int aurrekoJok = -1;
	static int aurrekoPotea = 0;
	//static int[] poteGuztiak = new int[500];//aldatu beharko da, imagenak zenbateraino gorde behar direnaren arabera
	static LinkedList<karta> nireKartak = new LinkedList<karta>();
	static LinkedList<karta> erdikoKartak = new LinkedList<karta>();
	static String nirePosizioa = "";
	static boolean[] zeinAusente = new boolean[6];



	public static boolean kartakEzagunakIzanArteEgon() throws Exception{

		while(true)
		{
			System.out.print("*");
			//kaptura atera 
			String FILENAME="irudiak/kapturaNereak.png";
			captureScreen(FILENAME);
			Thread.sleep(time1);

			String command = "convert irudiak/kapturaNereak.png -crop 18x42+896+617 -compress none irudiak/karta.pgm";
			Process proc = Runtime.getRuntime().exec(command);

			Thread.sleep(time1);

			File fitx = new File("irudiak/karta.pgm");
			Scanner in = new Scanner(fitx);

			String lag ="";
			int konp = -1;

			for(int i=0; i<25; i++)//123. begiratu beharrean hobea asmatu
			{
				lag = in.nextLine();
				if(i>20)
				{
					konp = Integer.parseInt(lag.split(" ")[5]);
					if(konp > 200){
						nagusia.jokatzenNago = true;
						return true;
					}
				}
			}
			in.close();
		}
	}

	public static boolean flopaIrtenArteEgon() throws Exception{


		while(nagusia.jokatzenNago)
		{
			System.out.print("*");
			//kaptura atera 
			String FILENAME="irudiak/kapturaFlop.png";
			captureScreen(FILENAME);
			Thread.sleep(time1);

			String command = "convert irudiak/kapturaFlop.png -crop 18x42+931+418 -compress none irudiak/karta.pgm";
			Process proc = Runtime.getRuntime().exec(command);

			Thread.sleep(time1);

			File fitx = new File("irudiak/karta.pgm");
			Scanner in = new Scanner(fitx);

			String lag ="";
			int konp = -1;

			for(int i=0; i<25; i++)//123. begiratu beharrean hobea asmatu
			{
				lag = in.nextLine();
				if(i>20)
				{
					konp = Integer.parseInt(lag.split(" ")[5]);
					if(konp > 200)return true;
				}
			}
			in.close();
			//	if(partidaBukatuDa2())return false;//bukatu ahal den begiratu		
		}
		return false;
	}

	public static boolean turnIrtenArteEgon() throws Exception{


		while(nagusia.jokatzenNago)
		{

			System.out.print("*");
			//kaptura atera 
			String FILENAME="irudiak/kapturaTurn.png";
			captureScreen(FILENAME);
			Thread.sleep(time1);

			String command = "convert irudiak/kapturaTurn.png -crop 18x42+996+418 -compress none irudiak/karta.pgm";
			Process proc = Runtime.getRuntime().exec(command);

			Thread.sleep(time1);

			File fitx = new File("irudiak/karta.pgm");
			Scanner in = new Scanner(fitx);

			String lag ="";
			int konp = -1;

			for(int i=0; i<25; i++)//123. begiratu beharrean hobea asmatu
			{
				lag = in.nextLine();
				if(i>20)
				{
					konp = Integer.parseInt(lag.split(" ")[5]);
					if(konp > 200)return true;
				}
			}
			in.close();
			if(partidaBukatuDa2())return false;//bukatu ahal den begiratu		}
		}
		return false;
	}

	public static boolean riverIrtenArteEgon() throws Exception{


		while(nagusia.jokatzenNago)
		{

			System.out.print("*");
			//kaptura atera 
			String FILENAME="irudiak/kapturaRiver.png";
			captureScreen(FILENAME);
			Thread.sleep(time1);

			String command = "convert irudiak/kapturaRiver.png -crop 18x42+1061+418 -compress none irudiak/karta.pgm";
			Process proc = Runtime.getRuntime().exec(command);

			Thread.sleep(time1);

			File fitx = new File("irudiak/karta.pgm");
			Scanner in = new Scanner(fitx);

			String lag ="";
			int konp = -1;

			for(int i=0; i<25; i++)//123. begiratu beharrean hobea asmatu
			{
				lag = in.nextLine();
				if(i>20)
				{
					konp = Integer.parseInt(lag.split(" ")[5]);
					if(konp > 200)return true;
				}
			}
			in.close();
			if(partidaBukatuDa2())return false;//bukatu ahal den begiratu
		}
		return false;

	}

	public static boolean partidaBukatuDa() throws Exception{

		while(true){

			System.out.print("*");
			//kaptura atera 
			String FILENAME="irudiak/kapturaBukatzeko.png";
			captureScreen(FILENAME);
			Thread.sleep(time1);

			String command = "convert irudiak/kapturaBukatzeko.png -crop 18x42+931+418 -compress none irudiak/karta.pgm";
			Process proc = Runtime.getRuntime().exec(command);

			Thread.sleep(time1);

			File fitx = new File("irudiak/karta.pgm");
			Scanner in = new Scanner(fitx);

			String lag ="";
			int konp = -1;
			boolean bukatu = true;

			for(int i=0; i<25; i++)//123. begiratu beharrean hobea asmatu
			{
				lag = in.nextLine();
				if(i>20)
				{
					konp = Integer.parseInt(lag.split(" ")[5]);
					if(konp > 200)bukatu = false;
				}
			}
			//bukatu behar bada bukatu, bestela jarraitu
			if(bukatu==true){
				zeMomentutan = "preFlop";//azkarrago egiteko
				return true;
			}

			in.close();
			Thread.sleep(time2);
		}
	}

	public static boolean partidaBukatuDa2() throws Exception{


		System.out.print("*");
		//kaptura atera 
		String FILENAME="irudiak/kapturaBukatzeko.png";
		captureScreen(FILENAME);
		Thread.sleep(time1);

		String command = "convert irudiak/kapturaBukatzeko.png -crop 18x42+931+418 -compress none irudiak/bukatzeko.pgm";
		Process proc = Runtime.getRuntime().exec(command);

		Thread.sleep(time1);

		File fitx = new File("irudiak/bukatzeko.pgm");
		Scanner in = new Scanner(fitx);

		String lag ="";
		int konp = -1;
		boolean bukatu = true;

		for(int i=0; i<25; i++)//123. begiratu beharrean hobea asmatu
		{
			lag = in.nextLine();
			if(i>20)
			{
				konp = Integer.parseInt(lag.split(" ")[5]);
				if(konp > 200)bukatu = false;
			}
		}
		in.close();

		//bukatu behar bada bukatu, bestela jarraitu
		if(bukatu){
			zeMomentutan = "preFlop";
			return true;
		}
		else return false;

	}

	static public void ezagunarenKartakLortu() throws IOException, InterruptedException
	{
		System.out.println("KARTAK BANATUTA");

		String command = "convert irudiak/kapturaNereak.png -crop 18x42+897+616 -compress none irudiak/karta6.pgm";//|| 896.5
		Process proc = Runtime.getRuntime().exec(command);
		command = "convert irudiak/kapturaNereak.png -crop 18x42+958+616 -compress none irudiak/karta7.pgm";
		proc = Runtime.getRuntime().exec(command);

		Thread.sleep(time1);

		command = "python irudiak/pythonKartak/toarffTest6.py";
		proc = Runtime.getRuntime().exec(command);
		command = "python irudiak/pythonKartak/toarffTest7.py";
		proc = Runtime.getRuntime().exec(command);

		Thread.sleep(time1);

		command = "python irudiak/pythonKartak/weka6.py";
		proc = Runtime.getRuntime().exec(command);
		command = "python irudiak/pythonKartak/weka7.py";
		proc = Runtime.getRuntime().exec(command);

		Thread.sleep(time3+1000);

		File fitx_6 = new File("irudiak/pythonKartak/erantzunaKarta6.txt");
		Scanner in_6 = new Scanner(fitx_6);
		int karta6 = in_6.nextInt();

		File fitx_7 = new File("irudiak/pythonKartak/erantzunaKarta7.txt");
		Scanner in_7 = new Scanner(fitx_7);
		int karta7 = in_7.nextInt();

		nireKartak = new LinkedList<karta>();
		nireKartak.add(zeinKartaDa(karta6));
		nireKartak.add(zeinKartaDa(karta7));

		pythonKartakFitxategiaEzabatu(6);
		pythonKartakFitxategiaEzabatu(7);

	}

	public static void flopekoKartakLortu() throws IOException, InterruptedException 
	{



		System.out.println("FLOP");
		String command = "convert irudiak/kapturaFlop.png -crop 18x42+801.5+417 -compress none irudiak/karta1.pgm";
		Process proc = Runtime.getRuntime().exec(command);
		command = "convert irudiak/kapturaFlop.png -crop 18x42+866+417 -compress none irudiak/karta2.pgm";
		proc = Runtime.getRuntime().exec(command);
		command = "convert irudiak/kapturaFlop.png -crop 18x42+931+417 -compress none irudiak/karta3.pgm";
		proc = Runtime.getRuntime().exec(command);

		Thread.sleep(time2);

		command = "python irudiak/pythonKartak/toarffTest1.py";
		proc = Runtime.getRuntime().exec(command);
		command = "python irudiak/pythonKartak/toarffTest2.py";
		proc = Runtime.getRuntime().exec(command);
		command = "python irudiak/pythonKartak/toarffTest3.py";
		proc = Runtime.getRuntime().exec(command);

		Thread.sleep(time2);

		command = "python irudiak/pythonKartak/weka1.py";
		proc = Runtime.getRuntime().exec(command);
		command = "python irudiak/pythonKartak/weka2.py";
		proc = Runtime.getRuntime().exec(command);
		command = "python irudiak/pythonKartak/weka3.py";
		proc = Runtime.getRuntime().exec(command);

		Thread.sleep(time3+1500);


		File fitx_1 = new File("irudiak/pythonKartak/erantzunaKarta1.txt");
		Scanner in_1 = new Scanner(fitx_1);
		int karta1 = in_1.nextInt();

		File fitx_2 = new File("irudiak/pythonKartak/erantzunaKarta2.txt");
		Scanner in_2 = new Scanner(fitx_2);
		int karta2 = in_2.nextInt();

		File fitx_3 = new File("irudiak/pythonKartak/erantzunaKarta3.txt");
		Scanner in_3 = new Scanner(fitx_3);
		int karta3 = in_3.nextInt();

		erdikoKartak = new LinkedList<karta>();
		erdikoKartak.add(zeinKartaDa(karta1));
		erdikoKartak.add(zeinKartaDa(karta2));
		erdikoKartak.add(zeinKartaDa(karta3));

		pythonKartakFitxategiaEzabatu(1);
		pythonKartakFitxategiaEzabatu(2);
		pythonKartakFitxategiaEzabatu(3);


	}

	public static void turnekoKartakLortu() throws IOException, InterruptedException
	{
		System.out.println("TURN");
		String command = "convert irudiak/kapturaTurn.png -crop 18x42+996+417 -compress none irudiak/karta4.pgm";
		Process proc = Runtime.getRuntime().exec(command);

		Thread.sleep(time1);

		command = "python irudiak/pythonKartak/toarffTest4.py";
		proc = Runtime.getRuntime().exec(command);

		Thread.sleep(time1);

		command = "python irudiak/pythonKartak/weka4.py";
		proc = Runtime.getRuntime().exec(command);

		Thread.sleep(time3);

		File fitx_4 = new File("irudiak/pythonKartak/erantzunaKarta4.txt");
		Scanner in_4 = new Scanner(fitx_4);
		int karta4 = in_4.nextInt();

		erdikoKartak.add(zeinKartaDa(karta4));	
		pythonKartakFitxategiaEzabatu(4);

	}

	public static void riverrekoKartakLortu() throws Exception
	{
		System.out.println("RIVER");
		String command = "convert irudiak/kapturaRiver.png -crop 18x42+1061+417 -compress none irudiak/karta5.pgm";
		Process proc = Runtime.getRuntime().exec(command);

		Thread.sleep(time1);

		command = "python irudiak/pythonKartak/toarffTest5.py";
		proc = Runtime.getRuntime().exec(command);

		Thread.sleep(time1);

		command = "python irudiak/pythonKartak/weka5.py";
		proc = Runtime.getRuntime().exec(command);

		Thread.sleep(time3);

		File fitx_5 = new File("irudiak/pythonKartak/erantzunaKarta5.txt");
		Scanner in_5 = new Scanner(fitx_5);
		int karta5 = in_5.nextInt();

		erdikoKartak.add(zeinKartaDa(karta5));
		pythonKartakFitxategiaEzabatu(5);


		partidaBukatuDa();
		System.out.println("Partida bukatu da");
	}

	public static void captureScreen(String fileName) throws Exception {

		Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		Rectangle screenRectangle = new Rectangle(screenSize);
		Robot robot = new Robot();
		BufferedImage image = robot.createScreenCapture(screenRectangle);
		ImageIO.write(image, "png", new File(fileName));
	}

	private static karta zeinKartaDa(int num)
	{

		String paloa;
		String numString;
		int numeroa = num % 13;

		if(numeroa==11)numString = "J";
		else if(numeroa==12)numString = "Q";
		else if(numeroa==0)numString = "K";
		else if(numeroa==1)numString = "A";
		else numString = String.valueOf(numeroa);

		if(num <= 13)paloa = "d";//diamonts
		else if(num <= 26)paloa = "h";//heart
		else if(num <= 39)paloa = "c";//clubs
		else paloa = "s";//spades

		//	System.out.println(num+" ****** Num = "+numString+"    Paloa = "+paloa);
		if(numeroa == 0)numeroa = 13;

		return new karta(numString, paloa,numeroa);
	}

	//	public static void ciegaHandiaLortu(int potea) throws InterruptedException
	//	{
	//		ciegaHartu = false;
	//		//ciega lortu
	//		if(ciegaHandia == -1 || aurrekoCiega == potea)
	//		{
	//			ciegaHandia = (potea-(ante*6))*2/3;//jendea kalera joan al den begiratu behar
	//		}
	//		else if(aurrekoCiega < potea)
	//		{
	//			ante += 1;//torneoaren arabera
	//			ciegaHandia = (potea-(ante*6))*2/3;
	//		}
	//		else System.out.println("ERROR");
	//
	//		aurrekoCiega = potea;
	//		System.out.println("Ciega handia = "+ciegaHandia);
	//	}

	public static void potetikInformazioaLortu(int kapturaZenb) throws Exception
	{

		//zenbatgarren karpetan kalkulatuko den atera
		int i = (kapturaZenb % 5);
		if(i==0)i=5;

		//potea zenbatekoa den irakurri
		int potea = poteaIrakurri(kapturaZenb, i);
		//jokatzen hari den jokalaria momentu horretan
		int zein = zeinHariDaJokatzenErantzunaHartu(i);

		//	System.out.println("kaptura"+kapturaZenb+"    potea="+potea+"    ("+zein+")");

		if(zein != aurrekoJok)
		{
			pausoa p = zerGertatuDa(potea, aurrekoPotea, aurrekoJok, ciegaHandia);
			System.out.println(p.getZeinek().getPosizioa()+"  "+p.getZer()+"  "+p.getZenbat());
			zerGertatuDa.add(p);
		}

		//nire txanda da
		if(zein == 0 && zein != aurrekoJok)
		{
			System.out.println("NIRE TXANDA");
			
			aurrekoJok = zein;
			nagusia.nireTxanda = true;

			zerEgin zer = new zerEgin();
			//lehenengo aldian bakarrik lortu modu honetan nire posizioa
			if(nagusia.lehenAldia){
				nagusia.lehenAldia = false;
				nirePosizioa = zer.zeinPosiziotanNago(zerGertatuDa);
			}
			else
				funtzioak.nirePosizioa = funtzioak.posizioaAldatu(funtzioak.nirePosizioa);
			
			String erantzuna = zer.nireTxandanZerEgin(ciegaHandia, zerGertatuDa, potea);

			//if(erantzuna.compareTo("fold")==0)nagusia.jokatzenNago=false;
			nagusia.nireTxanda = false;

			//aurreko informazioa ere behar bada  hau ezin da egin
			zerGertatuDa = new LinkedList<pausoa>();

		}

		//potea eguneratu
		aurrekoPotea = potea;
		aurrekoJok = zein;

	}

	private static pausoa zerGertatuDa(float potea, int aurrePotea, int pos, float ciegaHandia)
	{
		if(zeMomentutan.compareTo("preFlop")==0)
		{
			if(potea - aurrePotea == ciegaHandia)
			{
				player p = new player(false, pos+"", -1);
				return new pausoa(p, "call", (int) ciegaHandia);//ciega handia ondo kalulatu gabe, hau ez da gertatuko
			}
			else if(potea - aurrePotea > 0)
			{
				player p = new player(false, pos+"", -1);
				return new pausoa(p, "raise", (int) (potea-aurrePotea));
			}
			else if(potea == aurrePotea)
			{
				player p = new player(false, pos+"", -1);
				return new pausoa(p, "fold", 0);
			}
			else{//potea txikiagoa, zerbait gaizki
				player p = new player(false, pos+"", -1);
				return new pausoa(p, "NULL", 0);
			}
		}
		else if(zeMomentutan.compareTo("flop")==0 || zeMomentutan.compareTo("river")==0 || zeMomentutan.compareTo("turn")==0)
		{

			if(potea - aurrePotea == raiseZenbat)
			{
				player p = new player(false, pos+"", -1);
				return new pausoa(p, "call", (int) ciegaHandia);
			}
			else if(potea > aurrePotea)
			{
				player p = new player(false, pos+"", -1);
				raiseEginda = true;
				raiseZenbat = (int) (potea-aurrePotea);
				return new pausoa(p, "raise", (int) (potea-aurrePotea));//ez dakit seguru horrela den REAIS
			}
			else if(potea == aurrePotea && !raiseEginda)
			{
				player p = new player(false, pos+"", -1);
				return new pausoa(p, "check", 0);
			}
			else if(potea == aurrePotea && raiseEginda)
			{
				player p = new player(false, pos+"", -1);
				return new pausoa(p, "fold", 0);
			}
			else{
				player p = new player(false, pos+"", -1);
				return new pausoa(p, "NULL", 0);
			}
		}

		player p = new player(false, pos+"", -1);
		return new pausoa(p, "NULL", 0);
	}

	public static int poteaIrakurri(int kapturaNum, int i) throws Exception
	{
		String poteaString = "";
		int luzera = -1;
		int potea = -1;
		String command;
		Process proc;
		int lag = kapturaNum;
		int zein = 0;


		//////////
		//LUZERA//
		/////////

		command = "convert irudiak/kaptura"+kapturaNum+".png -crop 190x4+876+380 -compress none irudiak/luzera"+i+".pgm";
		proc = Runtime.getRuntime().exec(command);


		Thread.sleep(time1);

		command = "python irudiak/pythonLuzera/toarffTest"+i+".py";
		proc = Runtime.getRuntime().exec(command);

		Thread.sleep(time1);

		command = "python irudiak/pythonLuzera/weka"+i+".py";
		proc = Runtime.getRuntime().exec(command);

		Thread.sleep(time3);

		File fitx = new File("irudiak/pythonLuzera/erantzuna_"+i+".txt");
		Scanner in = new Scanner(fitx);
		luzera = in.nextInt();


		//jokatzen hari den jokalaria momentu horretan
		zeinHariDaJokatzenEbaki(kapturaNum, i);

		//////////////
		// NUMEROAK //
		/////////////


		if(luzera==1){

			potea = -1;

		}
		else if(luzera==2){

			command = "convert irudiak/kaptura"+kapturaNum+".png -crop 10x16+965+383 -compress none irudiak/pythonNum/"+i+"/num1.pgm";
			proc = Runtime.getRuntime().exec(command);
			command = "convert irudiak/kaptura"+kapturaNum+".png -crop 10x16+975+383 -compress none irudiak/pythonNum/"+i+"/num2.pgm";
			proc = Runtime.getRuntime().exec(command);

			Thread.sleep(time1);

			command = "python irudiak/pythonNum/"+i+"/toarffTest1.py";
			proc = Runtime.getRuntime().exec(command);
			command = "python irudiak/pythonNum/"+i+"/toarffTest2.py";
			proc = Runtime.getRuntime().exec(command);

			Thread.sleep(time1);

			command = "python irudiak/pythonNum/"+i+"/weka1.py";
			proc = Runtime.getRuntime().exec(command);
			command = "python irudiak/pythonNum/"+i+"/weka2.py";
			proc = Runtime.getRuntime().exec(command);

			Thread.sleep(time3);

			File fitx_1 = new File("irudiak/pythonNum/"+i+"/erantzunaNum1.txt");
			Scanner in_1 = new Scanner(fitx_1);
			int num_1 = in_1.nextInt();

			File fitx_2 = new File("irudiak/pythonNum/"+i+"/erantzunaNum2.txt");
			Scanner in_2 = new Scanner(fitx_2);
			int num_2 = in_2.nextInt();

			poteaString = num_1+""+num_2+"";
			potea = Integer.valueOf(poteaString);


		}
		else if(luzera==3){

			command = "convert irudiak/kaptura"+kapturaNum+".png -crop 10x16+960+383 -compress none irudiak/pythonNum/"+i+"/num1.pgm";
			proc = Runtime.getRuntime().exec(command);
			command = "convert irudiak/kaptura"+kapturaNum+".png -crop 10x16+970+383 -compress none irudiak/pythonNum/"+i+"/num2.pgm";
			proc = Runtime.getRuntime().exec(command);
			command = "convert irudiak/kaptura"+kapturaNum+".png -crop 10x16+980+383 -compress none irudiak/pythonNum/"+i+"/num3.pgm";
			proc = Runtime.getRuntime().exec(command);

			Thread.sleep(time1);

			command = "python irudiak/pythonNum/"+i+"/toarffTest1.py";
			proc = Runtime.getRuntime().exec(command);
			command = "python irudiak/pythonNum/"+i+"/toarffTest2.py";
			proc = Runtime.getRuntime().exec(command);
			command = "python irudiak/pythonNum/"+i+"/toarffTest3.py";
			proc = Runtime.getRuntime().exec(command);

			Thread.sleep(time1);

			command = "python irudiak/pythonNum/"+i+"/weka1.py";
			proc = Runtime.getRuntime().exec(command);
			command = "python irudiak/pythonNum/"+i+"/weka2.py";
			proc = Runtime.getRuntime().exec(command);
			command = "python irudiak/pythonNum/"+i+"/weka3.py";
			proc = Runtime.getRuntime().exec(command);

			Thread.sleep(time3);

			File fitx_1 = new File("irudiak/pythonNum/"+i+"/erantzunaNum1.txt");
			Scanner in_1 = new Scanner(fitx_1);
			int num_1 = in_1.nextInt();

			File fitx_2 = new File("irudiak/pythonNum/"+i+"/erantzunaNum2.txt");
			Scanner in_2 = new Scanner(fitx_2);
			int num_2 = in_2.nextInt();

			File fitx_3 = new File("irudiak/pythonNum/"+i+"/erantzunaNum3.txt");
			Scanner in_3 = new Scanner(fitx_3);
			int num_3 = in_3.nextInt();

			poteaString = num_1+""+num_2+""+num_3+"";
			potea = Integer.valueOf(poteaString);


		}
		else if(luzera==4){

			command = "convert irudiak/kaptura"+kapturaNum+".png -crop 10x16+953+383 -compress none irudiak/pythonNum/"+i+"/num1.pgm";
			proc = Runtime.getRuntime().exec(command);
			command = "convert irudiak/kaptura"+kapturaNum+".png -crop 10x16+967+383 -compress none irudiak/pythonNum/"+i+"/num2.pgm";
			proc = Runtime.getRuntime().exec(command);
			command = "convert irudiak/kaptura"+kapturaNum+".png -crop 10x16+977+383 -compress none irudiak/pythonNum/"+i+"/num3.pgm";
			proc = Runtime.getRuntime().exec(command);
			command = "convert irudiak/kaptura"+kapturaNum+".png -crop 10x16+987+383 -compress none irudiak/pythonNum/"+i+"/num4.pgm";
			proc = Runtime.getRuntime().exec(command);

			Thread.sleep(time1);

			command = "python irudiak/pythonNum/"+i+"/toarffTest1.py";
			proc = Runtime.getRuntime().exec(command);
			command = "python irudiak/pythonNum/"+i+"/toarffTest2.py";
			proc = Runtime.getRuntime().exec(command);
			command = "python irudiak/pythonNum/"+i+"/toarffTest3.py";
			proc = Runtime.getRuntime().exec(command);
			command = "python irudiak/pythonNum/"+i+"/toarffTest4.py";
			proc = Runtime.getRuntime().exec(command);

			Thread.sleep(time1);

			command = "python irudiak/pythonNum/"+i+"/weka1.py";
			proc = Runtime.getRuntime().exec(command);
			command = "python irudiak/pythonNum/"+i+"/weka2.py";
			proc = Runtime.getRuntime().exec(command);
			command = "python irudiak/pythonNum/"+i+"/weka3.py";
			proc = Runtime.getRuntime().exec(command);
			command = "python irudiak/pythonNum/"+i+"/weka4.py";
			proc = Runtime.getRuntime().exec(command);


			Thread.sleep(time3);

			File fitx_1 = new File("irudiak/pythonNum/"+i+"/erantzunaNum1.txt");
			Scanner in_1 = new Scanner(fitx_1);
			int num_1 = in_1.nextInt();

			File fitx_2 = new File("irudiak/pythonNum/"+i+"/erantzunaNum2.txt");
			Scanner in_2 = new Scanner(fitx_2);
			int num_2 = in_2.nextInt();

			File fitx_3 = new File("irudiak/pythonNum/"+i+"/erantzunaNum3.txt");
			Scanner in_3 = new Scanner(fitx_3);
			int num_3 = in_3.nextInt();

			File fitx_4 = new File("irudiak/pythonNum/"+i+"/erantzunaNum4.txt");
			Scanner in_4 = new Scanner(fitx_4);
			int num_4 = in_4.nextInt();

			poteaString = num_1+""+num_2+""+num_3+""+num_4+"";
			potea = Integer.valueOf(poteaString);
		}
		else System.out.println("ERROR LUZERA");

		//	ciega hartu behar al den jakiteko
		//		poteGuztiak[kapturaNum]=potea;
		//		if(kapturaNum!=1)
		//			if((poteHutsaIzanDaAzkena(kapturaNum) || poteakBeraEginDu( kapturaNum)) && ciegaHartu)
		//				ciegaHandiaLortu(potea);

		pythonNumFitxategiakEzabatu(i);
		return potea;
	}


	public static void zeinHariDaJokatzenEbaki(int kapturaNum, int i) throws Exception
	{


		//jokatzen zein hari den jakiteko ebaki(6 jokalarirentzako)
		String command = "convert irudiak/kaptura"+kapturaNum+".png -crop 130x11+510+607 -compress none irudiak/player/"+i+"/player1.pgm";
		Process proc = Runtime.getRuntime().exec(command);
		command = "convert irudiak/kaptura"+kapturaNum+".png -crop 130x11+510+390 -compress none irudiak/player/"+i+"/player2.pgm";
		proc = Runtime.getRuntime().exec(command);
		command = "convert irudiak/kaptura"+kapturaNum+".png -crop 130x11+883+306 -compress none irudiak/player/"+i+"/player3.pgm";
		proc = Runtime.getRuntime().exec(command);
		command = "convert irudiak/kaptura"+kapturaNum+".png -crop 130x11+1279+390 -compress none irudiak/player/"+i+"/player4.pgm";
		proc = Runtime.getRuntime().exec(command);
		command = "convert irudiak/kaptura"+kapturaNum+".png -crop 130x11+1279+607 -compress none irudiak/player/"+i+"/player5.pgm";
		proc = Runtime.getRuntime().exec(command);
		command = "convert irudiak/kaptura"+kapturaNum+".png -crop 130x11+910+729 -compress none irudiak/player/"+i+"/player0.pgm";
		proc = Runtime.getRuntime().exec(command);

	}

	public static int zeinHariDaJokatzenErantzunaHartu(int karp) throws Exception
	{
		String lag = "";
		int konp = -1;;

		for(int j=0; j<6; j++)
		{
			File fitx = new File("irudiak/player/"+karp+"/player"+j+".pgm");
			Scanner in = new Scanner(fitx);

			for(int i=0; i<25; i++)//123. begiratu beharrean hobea asmatu
			{
				lag = in.nextLine();
				if(i>20)
				{
					konp = Integer.parseInt(lag.split(" ")[5]);
					if(konp > 100)return j;
				}
			}
			in.close();
		}
		return -1;//ez du jokatzen hari den inor aurkitu
	}

	public static void zeinDagoAusenteEbaki() throws Exception
	{

		//jokatzen zein hari den jakiteko ebaki(6 jokalarirentzako)
		String command = "convert irudiak/kapturaNereak.png -crop 20x20+529+520 -compress none irudiak/ausente/player1.pgm";
		Process proc = Runtime.getRuntime().exec(command);
		command = "convert irudiak/kapturaNereak.png -crop 20x20+529+302 -compress none irudiak/ausente/player2.pgm";
		proc = Runtime.getRuntime().exec(command);
		command = "convert irudiak/kapturaNereak.png -crop 20x20+905+218 -compress none irudiak/ausente/player3.pgm";
		proc = Runtime.getRuntime().exec(command);
		command = "convert irudiak/kapturaNereak.png -crop 20x20+1281+302 -compress none irudiak/ausente/player4.pgm";
		proc = Runtime.getRuntime().exec(command);
		command = "convert irudiak/kapturaNereak.png -crop 20x20+1344+519 -compress none irudiak/ausente/player5.pgm";
		proc = Runtime.getRuntime().exec(command);
//		command = "convert irudiak/kapturaNereak.png -crop 20x20+901+619 -compress none irudiak/ausente/player0.pgm";
//		proc = Runtime.getRuntime().exec(command);

	}

	public static boolean[] zeinDagoAusenteErantzunaHartu() throws Exception
	{
		int konp = -1;
		boolean[] erantzuna = new boolean[6];

		for(int j=1; j<6; j++)
		{
			File fitx = new File("irudiak/ausente/player"+j+".pgm");	
			konp = pixelakGehitu(fitx);
			//System.out.println("pixel"+j+" = "+konp);
			if(konp > 20000 && konp < 40000)erantzuna[j]=true;
			else erantzuna[j]=false;
		}	
		return erantzuna;
	}
	
	private static int pixelakGehitu(File fitx) throws FileNotFoundException{
		
		int pixel = 0;
		Scanner s = new Scanner(fitx);
		s.nextLine();
		s.nextLine();
		s.nextLine();
		String lag = "";
		for(int i=0; i<15; i++){
			lag = s.nextLine();
			String[] ilara=lag.split(" ");
			for(int j=0; j<ilara.length; j++){
				pixel += Integer.parseInt(ilara[j]);
			}
		}
		s.close();
		return pixel;
	}

	//	private static boolean poteHutsaIzanDaAzkena(int kaptura){
	//
	//		if(poteGuztiak[kaptura-1] == -1 && poteGuztiak[kaptura] !=-1){
	//
	//			System.out.println("[[[[POTE HUTSA AZKENA]]]");
	//			return true;
	//		}
	//		else
	//			return false;
	//
	//	}
	//
	//	private static boolean poteakBeraEginDu(int kaptura){
	//
	//		if(poteGuztiak[kaptura-1] > poteGuztiak[kaptura]){
	//
	//			System.out.println("[[[[POTEAK BERA EGIN DU]]]");
	//			return true;
	//		}
	//		else
	//			return false;
	//
	//	}

	public static void pythonNumFitxategiakEzabatu() throws IOException{

		for(int i=1; i<=5; i++)
		{
			for(int j=1;j<=4;j++)
			{
				String command = "rm irudiak/pythonNum/"+i+"/erantzuna"+j+".txt";
				Process proc = Runtime.getRuntime().exec(command);

				command = "rm irudiak/pythonNum/"+i+"/erantzunaNum"+j+".txt";
				proc = Runtime.getRuntime().exec(command);

				command = "rm irudiak/pythonNum/"+i+"/num"+j+".pgm";
				proc = Runtime.getRuntime().exec(command);

				command = "rm irudiak/pythonNum/"+i+"/numTest"+j+".arff";
				proc = Runtime.getRuntime().exec(command);
			}
		}
	}

	public static void pythonNumFitxategiakEzabatu(int i) throws IOException{


		for(int j=1;j<=4;j++)
		{
			String command = "rm irudiak/pythonNum/"+i+"/erantzuna"+j+".txt";
			Process proc = Runtime.getRuntime().exec(command);

			command = "rm irudiak/pythonNum/"+i+"/erantzunaNum"+j+".txt";
			proc = Runtime.getRuntime().exec(command);

			command = "rm irudiak/pythonNum/"+i+"/num"+j+".pgm";
			proc = Runtime.getRuntime().exec(command);

			command = "rm irudiak/pythonNum/"+i+"/numTest"+j+".arff";
			proc = Runtime.getRuntime().exec(command);
		}
	}

	public static void pythonLuzeraFitxategiakEzabatu() throws IOException{


		for(int j=1;j<=5;j++)
		{
			String command = "rm irudiak/pythonLuzera/erantzuna"+j+".txt";
			Process proc = Runtime.getRuntime().exec(command);

			command = "rm irudiak/pythonLuzera/erantzuna_"+j+".txt";
			proc = Runtime.getRuntime().exec(command);

			command = "rm irudiak/luzera"+j+".pgm";
			proc = Runtime.getRuntime().exec(command);

			command = "rm irudiak/pythonLuzera/luzeratest"+j+".arff";
			proc = Runtime.getRuntime().exec(command);
		}
	}

	public static void pythonKartakFitxategiakEzabatu() throws IOException{


		for(int j=1;j<=7;j++)
		{
			String command = "rm irudiak/pythonKartak/erantzuna"+j+".txt";
			Process proc = Runtime.getRuntime().exec(command);

			command = "rm irudiak/pythonKartak/erantzunaKarta"+j+".txt";
			proc = Runtime.getRuntime().exec(command);

			command = "rm irudiak/karta"+j+".pgm";
			proc = Runtime.getRuntime().exec(command);

			command = "rm irudiak/pythonKartak/kartaTest"+j+".arff";
			proc = Runtime.getRuntime().exec(command);
		}
	}
	private static void pythonKartakFitxategiaEzabatu(int j) throws IOException{

		String command = "rm irudiak/pythonKartak/erantzuna"+j+".txt";
		Process proc = Runtime.getRuntime().exec(command);

		command = "rm irudiak/pythonKartak/erantzunaKarta"+j+".txt";
		proc = Runtime.getRuntime().exec(command);

		command = "rm irudiak/karta"+j+".pgm";
		proc = Runtime.getRuntime().exec(command);

		command = "rm irudiak/pythonKartak/kartaTest"+j+".arff";
		proc = Runtime.getRuntime().exec(command);
	}

	public static String posizioaAldatu(String pos){

		zerEgin zer = new zerEgin();

		String pos1 = posizioaAldatuOrdenean(pos);
		String pos2 = zer.zeinPosiziotanNago(zerGertatuDa);

//		System.out.println("Pos 1 = "+pos1);
//		System.out.println("Pos 2 = "+pos2);

		if(pos2 == null)
			return pos1;
		else return pos2;
	}

	public static String posizioaAldatuOrdenean(String pos){

		if(pos.compareTo("UTG")==0)return "BB";
		else if(pos.compareTo("BB")==0)return "SB";
		else if(pos.compareTo("SB")==0)return "BU";
		else if(pos.compareTo("BU")==0)return "CO";
		else if(pos.compareTo("CO")==0)return "MP1";
		else if(pos.compareTo("MP1")==0)return "UTG";
		else return null;
	}
}
