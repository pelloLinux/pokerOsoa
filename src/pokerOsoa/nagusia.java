package pokerOsoa;

import java.util.LinkedList;

import objetuak.karta;
import objetuak.pausoa;

public class nagusia  extends Thread{

	static int i;
	static boolean nireTxanda;
	static boolean jokatzenNago;
	static boolean lehenAldia;

	
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		//fitxategiak ezabatu
		funtzioak.pythonNumFitxategiakEzabatu();
		funtzioak.pythonLuzeraFitxategiakEzabatu();
		funtzioak.pythonKartakFitxategiakEzabatu();
		
		//hasieratu
		nireTxanda = false;
		jokatzenNago = true;
		lehenAldia = true;
		i = 1;

		//konkurrenteki
		Thread thr1 = new Thread(kartakLortu);
		thr1.start();

		//kapturak atera
		while(true) {

			if(nireTxanda == false && jokatzenNago == true)//nire txanda bada ez egin horrelakorik
			{
				System.out.print(":");
				String FILENAME="irudiak/kaptura"+i+".png";
				funtzioak.captureScreen(FILENAME);

				if(funtzioak.zeMomentutan.compareTo("")!=0)
				{
					Thread thr2 = new Thread(potetikInforLortu);
					thr2.start();
				}

				//segundu erdiro kaptura bat atera
				Thread.sleep(650);

				//100 kaptura ateratakoan berriro hasi
				if(i==100)i=1;
				else i++;
			}
			//kaptura arteko denbora
			Thread.sleep(650);
		}
	}

	//KONKURRENTEKI EXEKUTATZEKO
	static Runnable kartakLortu = new Runnable() {
		public void run() {

			try {
				while(true)
				{

					funtzioak.zerGertatuDa = new LinkedList<pausoa>();
					funtzioak.nireKartak = new LinkedList<karta>();
					funtzioak.erdikoKartak = new LinkedList<karta>();
					funtzioak.zerGertatuDa = new LinkedList<pausoa>();
					funtzioak.kartakEzagunakIzanArteEgon();
					funtzioak.zeinDagoAusenteEbaki();
					funtzioak.ciegaHartu = true;
					funtzioak.ezagunarenKartakLortu();
					funtzioak.zeinAusente = funtzioak.zeinDagoAusenteErantzunaHartu();
					funtzioak.zeMomentutan = "preFlop";

					if(funtzioak.flopaIrtenArteEgon())
					{
						funtzioak.zerGertatuDa = new LinkedList<pausoa>();
						funtzioak.zeMomentutan = "flop";
						funtzioak.raiseEginda = false;
						funtzioak.flopekoKartakLortu();

						if(funtzioak.turnIrtenArteEgon())
						{
							funtzioak.zerGertatuDa = new LinkedList<pausoa>();
							funtzioak.zeMomentutan ="turn";
							funtzioak.raiseEginda = false;
							funtzioak.turnekoKartakLortu();

							if(funtzioak.riverIrtenArteEgon())
							{
								funtzioak.zerGertatuDa = new LinkedList<pausoa>();
								funtzioak.zeMomentutan = "river";
								funtzioak.raiseEginda = false;
								funtzioak.riverrekoKartakLortu();

							}else{
								System.out.println("Partida bukatu da");
								
							}
						}else{
							System.out.println("Partida bukatu da");
							
						}
					}else{
						System.out.println("Partida bukatu da");
						
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	};

	//KONKURRENTEKI EXEKUTATZEKO
	static Runnable potetikInforLortu = new Runnable() {
		public void run() {

			try {

				funtzioak.potetikInformazioaLortu(i);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	};
}

/**
 * Ciega handia naizenean batzuetan ezin du fold egin
 * 
 */
