package pokerOsoa;



import java.util.LinkedList;

import objetuak.jugada;
import objetuak.karta;


public class handEvaluator {

	public static LinkedList<jugada> zeinProiektuDitut(LinkedList<karta> l)
	{
		//aldatzeko
		LinkedList<karta> nireKartak = new LinkedList<karta>();
		nireKartak.add(l.get(0));
		nireKartak.add(l.get(1));
		

		//ordenatutakoarekin gaizki dabililelako
//		LinkedList<karta> list = new LinkedList<karta>();
//		for(int i=0; i<l.size(); i++)list.add(l.get(0));

		LinkedList<karta> erantzuna;
		LinkedList<karta> erantzuna2;
		LinkedList<jugada> itzultzeko = new LinkedList<jugada>();
		long startTime;
		long endTime;

		///////////ESKALERA ABIERTA//////////////////////////////////////////
		startTime = System.currentTimeMillis();
		erantzuna = eskaleraAbiertaDago(l.size(), l);
		endTime = System.currentTimeMillis();
		if(erantzuna!=null){

			erantzuna2 = nireZenbatKartaDaude(erantzuna, nireKartak);
			System.out.println("Eskalera irekia dago, nire "+erantzuna2.size()+" karta");
			itzultzeko.add(new jugada("eskalera", erantzuna.get(erantzuna.size()-1).getNum(),erantzuna, erantzuna2.size()));

		}
		///////////ESKALERA CERRADA//////////////////////////////////////////
		startTime = System.currentTimeMillis();
		erantzuna = eskaleraItxiaDago(l.size(), l);
		endTime = System.currentTimeMillis();
		if(erantzuna!=null){
			//			System.out.println("Nire artak");
			//			for(int i=0; i<nireKartak.size(); i++)System.out.println(nireKartak.get(i).getNum());
			//			System.out.println("Erantzuna");
			//			for(int i=0; i<erantzuna.size(); i++)System.out.println(erantzuna.get(i).getNum());
			erantzuna2 = nireZenbatKartaDaude(erantzuna, nireKartak);
			System.out.println("Eskalera itxia dago, nire "+erantzuna2.size()+" karta");
			itzultzeko.add(new jugada("eskalera", erantzuna.get(erantzuna.size()-1).getNum(),erantzuna, erantzuna2.size()));

		}
		///////////KOLOR PROIEKTO//////////////////////////////////////////
		startTime = System.currentTimeMillis();
		erantzuna = kolorProiektuaDago(l.size(), l);
		endTime = System.currentTimeMillis();
		if(erantzuna!=null){
			erantzuna2 = nireZenbatKartaDaude(erantzuna, nireKartak);
			System.out.println("Kolor proiektua dago, nire "+erantzuna2.size()+" karta");
			itzultzeko.add(new jugada("eskalera", erantzuna.get(erantzuna.size()-1).getNum(),erantzuna, erantzuna2.size()));

		}
		return itzultzeko;
	}

	public static jugada zeinJugadaDut(LinkedList<karta> l){

		long startTime;
		long endTime;
		LinkedList<karta> erantzuna;
		LinkedList<karta> erantzuna2;

		//aldatzeko
		LinkedList<karta> nireKartak = new LinkedList<karta>();
		nireKartak.add(l.get(0));
		nireKartak.add(l.get(1));
		


		///////////ESKALERA KOLOR//////////////////////////////////////////
		startTime = System.currentTimeMillis();
		erantzuna = eskaleraKolorDago(l.size(), l);
		endTime = System.currentTimeMillis();
		if(erantzuna!=null){
			erantzuna2 = nireZenbatKartaDaude(erantzuna, nireKartak);
			System.out.println("Eskalera kolor dago, nire "+erantzuna2.size()+" karta");	
			return new jugada("eskalera", erantzuna.get(erantzuna.size()-1).getNum(),erantzuna, erantzuna2.size());
		}
		else{

			///////////POKER//////////////////////////////////////////
			startTime = System.currentTimeMillis();
			erantzuna= pokerDago(l.size(), l);
			endTime = System.currentTimeMillis();
			if(erantzuna!=null){
				erantzuna2 = nireZenbatKartaDaude(erantzuna, nireKartak);
				System.out.println("Poker dago, nire "+erantzuna2.size()+" karta");
				return new jugada("eskalera", erantzuna.get(erantzuna.size()-1).getNum(),erantzuna, erantzuna2.size());

			}
			else{

				///////////FULL//////////////////////////////////////////
				startTime = System.currentTimeMillis();
				erantzuna = fullDago(l.size(), l);
				endTime = System.currentTimeMillis();
				if(erantzuna!=null){
					erantzuna2 = nireZenbatKartaDaude(erantzuna, nireKartak);
					System.out.println("Full dago, nire "+erantzuna2.size()+" karta");
					return new jugada("eskalera", erantzuna.get(erantzuna.size()-1).getNum(),erantzuna, erantzuna2.size());

				}
				else{

					//////////KOLOR///////////////////////////////////////////
					startTime = System.currentTimeMillis();
					erantzuna = kolorDago(l.size(), l);
					endTime = System.currentTimeMillis();
					if(erantzuna!=null){
						erantzuna2 = nireZenbatKartaDaude(erantzuna, nireKartak);
						System.out.println("Kolor dago, nire "+erantzuna2.size()+" karta");
						return new jugada("eskalera", erantzuna.get(erantzuna.size()-1).getNum(),erantzuna, erantzuna2.size());

					}
					else{

						///////////ESKALERA//////////////////////////////////////////
						startTime = System.currentTimeMillis();
						erantzuna = eskaleraDago(l.size(), l);
						endTime = System.currentTimeMillis();
						if(erantzuna!=null){
							erantzuna2 = nireZenbatKartaDaude(erantzuna, nireKartak);
							if(erantzuna2.size()==2 && nireKartak.getFirst().getBalioa()==nireKartak.getLast().getBalioa())
								erantzuna2.removeLast();
							System.out.println("Eskalera dago, nire "+erantzuna2.size()+" karta");
							return new jugada("eskalera", erantzuna.get(erantzuna.size()-1).getNum(),erantzuna, erantzuna2.size());

						}
						else{

							///////////TRIO//////////////////////////////////////////
							startTime = System.currentTimeMillis();
							erantzuna = trioDago(l.size(), l);
							endTime = System.currentTimeMillis();
							if(erantzuna!=null){
								erantzuna2 = nireZenbatKartaDaude(erantzuna, nireKartak);
								System.out.println("Trio dago, nire "+erantzuna2.size()+" karta");
								return new jugada("eskalera", erantzuna.get(erantzuna.size()-1).getNum(),erantzuna, erantzuna2.size());

							}
							else{

								///////////PARE BIKOITZA//////////////////////////////////////////
								startTime = System.currentTimeMillis();
								erantzuna = pareBikoitzaDago(l.size(), l);
								endTime = System.currentTimeMillis();
								if(erantzuna!=null){
									erantzuna2 = nireZenbatKartaDaude(erantzuna, nireKartak);
									System.out.println("Pare bikoitza dago, nire "+erantzuna2.size()+" karta");
									return new jugada("eskalera", erantzuna.get(erantzuna.size()-1).getNum(),erantzuna, erantzuna2.size());

								}
								else {

									///////////PAREJA////////////////////////////////////////
									startTime = System.currentTimeMillis();
									erantzuna = parejaDago(l.size(), l);
									endTime = System.currentTimeMillis();

									if(erantzuna!=null){
										erantzuna2 = nireZenbatKartaDaude(erantzuna, nireKartak);
										System.out.println("Pareja dago, nire "+erantzuna2.size()+" karta");
										return new jugada("eskalera", erantzuna.get(erantzuna.size()-1).getNum(),erantzuna, erantzuna2.size());


									}
									else System.out.println("Ez dago parejarik");
									//////////////////////////////////////////////////////////////

								}
							}
						}
					}
				}
			}
		}
		return new jugada();
	}

	private static LinkedList<karta> parejaDago(int k, LinkedList<karta> l)
	{
		for(int i=0; i<k-1; i++)
		{
			for(int j=i+1; j<k; j++)
			{
				if(l.get(i).getNum().compareTo(l.get(j).getNum())==0){

					LinkedList<karta> erantzuna = new LinkedList<karta>();
					erantzuna.add(l.get(i));
					erantzuna.add(l.get(j));
					return erantzuna;
				}
			}
		}
		//null bada ez dago parejarik
		return null;
	}

	private static LinkedList<karta> trioDago(int k, LinkedList<karta> l)
	{
		for(int i=0; i<k-2; i++)
		{
			for(int j=i+1; j<k-1; j++)
			{
				if(l.get(i).getNum().compareTo(l.get(j).getNum())==0){

					for(int w=j+1; w<k; w++){

						if(l.get(i).getNum().compareTo(l.get(w).getNum())==0){

							LinkedList<karta> erantzuna = new LinkedList<karta>();
							erantzuna.add(l.get(i));
							erantzuna.add(l.get(j));
							erantzuna.add(l.get(w));
							return erantzuna;
						}
					}
				}
			}
		}
		//null bada ez dago triorik
		return null;
	}

	private static LinkedList<karta> pokerDago(int k, LinkedList<karta> l)
	{
		for(int i=0; i<k-3; i++)
		{
			for(int j=i+1; j<k-2; j++)
			{
				if(l.get(i).getNum().compareTo(l.get(j).getNum())==0){

					for(int w=j+1; w<k-1; w++){

						if(l.get(i).getNum().compareTo(l.get(w).getNum())==0){

							for(int e=w+1; e<k; e++){

								if(l.get(w).getNum().compareTo(l.get(e).getNum())==0){

									LinkedList<karta> erantzuna = new LinkedList<karta>();
									erantzuna.add(l.get(i));
									erantzuna.add(l.get(j));
									erantzuna.add(l.get(w));
									erantzuna.add(l.get(e));
									return erantzuna;
								}
							}
						}
					}
				}
			}
		}
		//null bada ez dago pokerrik
		return null;
	}

	private static LinkedList<karta>  kolorDago(int k, LinkedList<karta> l)
	{
		for(int i=0; i<k-4; i++)
		{
			for(int j=i+1; j<k-3; j++)
			{
				if(l.get(i).getPalo().compareTo(l.get(j).getPalo())==0){

					for(int w=j+1; w<k-2; w++){

						if(l.get(i).getPalo().compareTo(l.get(w).getPalo())==0){

							for(int e=w+1; e<k-1; e++){

								if(l.get(i).getPalo().compareTo(l.get(e).getPalo())==0){

									for(int d=e+1; d<k; d++){

										if(l.get(i).getPalo().compareTo(l.get(d).getPalo())==0){

											LinkedList<karta> erantzuna = new LinkedList<karta>();
											erantzuna.add(l.get(i));
											erantzuna.add(l.get(j));
											erantzuna.add(l.get(w));
											erantzuna.add(l.get(e));
											erantzuna.add(l.get(d));
											return erantzuna;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		//null bada ez dago kolorrik
		return null;
	}

	private static LinkedList<karta> kolorProiektuaDago(int k, LinkedList<karta> l)
	{
		for(int i=0; i<k-4; i++)
		{
			for(int j=i+1; j<k-3; j++)
			{
				if(l.get(i).getPalo().compareTo(l.get(j).getPalo())==0){

					for(int w=j+1; w<k-2; w++){

						if(l.get(i).getPalo().compareTo(l.get(w).getPalo())==0){

							for(int e=w+1; e<k-1; e++){

								if(l.get(i).getPalo().compareTo(l.get(e).getPalo())==0){

									LinkedList<karta> erantzuna = new LinkedList<karta>();
									erantzuna.add(l.get(i));
									erantzuna.add(l.get(j));
									erantzuna.add(l.get(w));
									erantzuna.add(l.get(e));
									return erantzuna;

								}
							}
						}
					}
				}
			}
		}
		//null bada ez dago kolorrik
		return null;
	}
	private static LinkedList<karta> fullDago(int k, LinkedList<karta> list){

		LinkedList<karta> erantzuna;
		LinkedList<karta> erantzuna2;

		LinkedList<karta> l = new LinkedList<karta>();
		for(int i=0; i<k; i++)l.add(list.get(i));

		erantzuna = trioDago(k, l);
		if(erantzuna!=null){
			l.remove(erantzuna.get(0));
			l.remove(erantzuna.get(1));
			l.remove(erantzuna.get(2));

			erantzuna2 = parejaDago(k-3, l);
			if(erantzuna2!=null){
				erantzuna.add(erantzuna2.get(0));
				erantzuna.add(erantzuna2.get(1));
				return erantzuna;
			}
		}
		return null;
	}

	private static LinkedList<karta> pareBikoitzaDago(int k, LinkedList<karta> list){

		LinkedList<karta> erantzuna;
		LinkedList<karta> erantzuna2;

		LinkedList<karta> l = new LinkedList<karta>();
		for(int i=0; i<k; i++)l.add(list.get(i));

		erantzuna = parejaDago(k, l);
		if(erantzuna!=null){
			l.remove(erantzuna.get(0));
			l.remove(erantzuna.get(1));
			erantzuna2 = parejaDago(k-2, l);
			if(erantzuna2!=null){

				erantzuna.add(erantzuna2.get(0));
				erantzuna.add(erantzuna2.get(1));
				return erantzuna;
			}
		}
		return null;
	}

	private static LinkedList<karta> eskaleraDago(int k, LinkedList<karta> list){

		//kartak ordenatu
		selectionSort(list);

		LinkedList<karta> erantzuna = new LinkedList<karta>();

		int kont = 0;
		boolean eskalera = false;
		for(int i=1; i<k; i++)
		{
			if(list.get(i).getBalioa()-list.get(i-1).getBalioa()==0){}
			if(list.get(i).getBalioa()-list.get(i-1).getBalioa()==1){

				if(kont<4)
				{
					erantzuna.add(list.get(i-1));
					kont++;
				}
				//Ass-akin bukatzen den eskalera begiratu
				if(kont==3 && list.get(i).getBalioa()==13)
				{
					if(list.get(0).getBalioa()==1){
						erantzuna.add(list.get(i));
						kont++;
						erantzuna.add(list.get(0));
						kont++;
					}
				}
				if(kont>=4){
					eskalera = true;
					erantzuna.add(list.get(i));
					kont++;
				}
			}
			else {
				if(eskalera==false)
				{
					erantzuna = new LinkedList<karta>();
					kont=0;
				}
			}	
		}	
		if(kont>=5){

			return erantzuna;
		}
		else return null;
	}

	//Ass-a atzetik jarri
	private static LinkedList<karta> eskaleraItxiaDago(int k, LinkedList<karta> list){

		LinkedList<karta> erantzuna = new LinkedList<karta>();

		//kartak ordenatu
		selectionSort(list);

		//lehengoa Ass-a bada bukaeran gehitu 14 bezala
		if(list.get(0).getBalioa()==1)list.add(new karta(list.get(0).getNum(), list.get(0).getPalo(), 14));


		int kont = 0;
		boolean eskalera = false;
		boolean zuloa = true;

		for(int i=1; i<list.size(); i++)
		{
			if(kont == 3){
				erantzuna.add(list.get(i-1));
				eskalera = true;
				break;
			}
			else if(list.get(i).getBalioa()-list.get(i-1).getBalioa()==0){}
			else if(list.get(i).getBalioa()-list.get(i-1).getBalioa()==1){

				kont++;
				erantzuna.add(list.get(i-1));
				//if(kont==1)zuloa=true;
			}
			else if(list.get(i).getBalioa()-list.get(i-1).getBalioa()==2 && zuloa){

				kont++;
				erantzuna.add(list.get(i-1));
				zuloa=false;
			}
			else{

				erantzuna = new LinkedList<karta>();
				kont=0;
				zuloa = false;
			}
		}
		//lehengoa Ass-a bada bukaeran ezabatu 14 bezala
		if(list.get(0).getBalioa()==1)list.removeLast();

		if(eskalera || kont==3){

			return erantzuna;
		}
		else return null;
	}

	private static LinkedList<karta> eskaleraAbiertaDago(int k, LinkedList<karta> list){


		LinkedList<karta> erantzuna = new LinkedList<karta>();

		//kartak ordenatu
		selectionSort(list);


		int kont = 0;
		boolean eskalera = false;

		for(int i=1; i<k; i++)
		{
			if(list.get(i).getBalioa()-list.get(i-1).getBalioa()==1){

				if(kont<3)
				{
					erantzuna.add(list.get(i-1));
					kont++;
				}
				//Ass-akin bukatzen den eskalera begiratu
				if(kont==2 && list.get(i).getBalioa()==13)
				{
					if(list.get(0).getBalioa()==1){
						erantzuna.add(list.get(i));
						kont++;
						erantzuna.add(list.get(0));
						kont++;
					}
				}
				if(kont>=3){
					eskalera = true;
					erantzuna.add(list.get(i));
					kont++;
				}
			}
			else {
				if(eskalera==false)
				{
					erantzuna = new LinkedList<karta>();
					kont=0;
				}
			}	
		}	
		if(kont>=4){


			return erantzuna;
		}
		else return null;
	}

	private static LinkedList<karta> eskaleraKolorDago(int k, LinkedList<karta> list){

		LinkedList<karta> erantzuna = new LinkedList<karta>();

		//kartak ordenatu
		selectionSort(list);

		int kont = 0;
		boolean eskalera = false;
		for(int i=1; i<k; i++)
		{
			if(list.get(i).getBalioa()-list.get(i-1).getBalioa()==0){}
			else if(list.get(i).getBalioa()-list.get(i-1).getBalioa()==1 && list.get(i).getPalo().compareTo(list.get(i-1).getPalo())==0){

				if(kont<4)
				{
					erantzuna.add(list.get(i-1));
					kont++;
				}
				//Ass-akin bukatzen den eskalera begiratu
				if(kont==3 && list.get(i).getBalioa()==13 && list.get(i).getPalo().compareTo(list.get(i-1).getPalo())==0)
				{
					if(list.get(0).getBalioa()==1 && list.get(0).getPalo().compareTo(list.get(i).getPalo())==0){
						erantzuna.add(list.get(i));
						kont++;
						erantzuna.add(list.get(0));
						kont++;
					}
				}
				if(kont>=4){
					eskalera = true;
					erantzuna.add(list.get(i));
					kont++;
				}
			}
			else {
				if(eskalera==false)
				{
					erantzuna = new LinkedList<karta>();
					kont=0;
				}
			}	
		}	
		if(kont>=5){

			return erantzuna;
		}
		else return null;
	}

	public static void selectionSort(LinkedList<karta> l) {

		int i, j, minIndex;
		karta tmp;
		int n = l.size();
		for (i = 0; i < n - 1; i++) {
			minIndex = i;
			for (j = i + 1; j < n; j++)
				if (l.get(j).getBalioa() < l.get(minIndex).getBalioa())
					minIndex = j;
			if (minIndex != i) {
				tmp = l.get(i);
				l.set(i, l.get(minIndex));
				l.set(minIndex, tmp);
			}
		}
	}

	private static LinkedList<karta> nireZenbatKartaDaude(LinkedList<karta> l, LinkedList<karta> nireKartak){

		LinkedList<karta> erantzuna = new LinkedList<karta>();
		
		//lehenengo karta
		for(int i =0; i<l.size(); i++)
			if(nireKartak.getFirst().getNum().compareTo(l.get(i).getNum())==0 && nireKartak.getFirst().getPalo().compareTo(l.get(i).getPalo())==0)
				erantzuna.add(nireKartak.getFirst());
		//bigarren karta
		for(int i =0; i<l.size(); i++)
			if(nireKartak.getLast().getNum().compareTo(l.get(i).getNum())==0 && nireKartak.getLast().getPalo().compareTo(l.get(i).getPalo())==0)
				erantzuna.add(nireKartak.getLast());
	
		return erantzuna;
	}
	
	private static String ikonoaBueltatu(String palo){

		if(palo.compareTo("d")==0)
			return "♦";
		else if(palo.compareTo("h")==0)
			return "♥";
		else if(palo.compareTo("s")==0)
			return "♠";
		else if(palo.compareTo("c")==0)
			return "♣";
		else System.err.println("[ERROREA]");
		return "Errorea";
	}

}