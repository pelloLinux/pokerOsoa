package pokerOsoa;

import java.awt.AWTException;
import java.util.LinkedList;

import objetuak.jugada;
import objetuak.karta;
import objetuak.pausoa;

public class zerEgin {

	private xaguaMugitu xagua = new xaguaMugitu();

	public String nireTxandanZerEgin(int ciegaHandia, LinkedList<pausoa> zerGertatuDa, int potea) throws AWTException, InterruptedException{

		jugada j = null;//osatutako jugadak
		LinkedList<jugada> p = null;//karta bat falta (proiektuak)

		if(funtzioak.zeMomentutan.compareTo("preFlop")==0)
		{
			pausoa paus = zerEginPreFlop(ciegaHandia, zerGertatuDa, potea, funtzioak.nirePosizioa);
			informazioaAzaleratu(ciegaHandia, zerGertatuDa, potea, 0);
			return xaguaMugitu(paus);

		}
		else if(funtzioak.zeMomentutan.compareTo("flop")==0)
		{
			j = zeinJugadaDut(5);
			p = zeinProiektuDut(5);
			//harien errore bat saiezteko
			if(p==null || j==null)return null;
			pausoa paus = zerEginFlop(ciegaHandia, zerGertatuDa,j,p, potea, funtzioak.nirePosizioa);
			informazioaAzaleratu(ciegaHandia, zerGertatuDa, potea, 3);
			return xaguaMugitu(paus);

		}
		else if(funtzioak.zeMomentutan.compareTo("turn")==0)
		{
			j = zeinJugadaDut(6);
			p = zeinProiektuDut(6);
			//harien errore bat saiezteko
			if(p==null || j==null)return null;
			pausoa paus = zerEginTurn(ciegaHandia, zerGertatuDa,j,p, potea, funtzioak.nirePosizioa);
			informazioaAzaleratu(ciegaHandia, zerGertatuDa, potea, 4);
			return xaguaMugitu(paus);

		}
		else if(funtzioak.zeMomentutan.compareTo("river")==0)
		{
			j = zeinJugadaDut(7);
			p = zeinProiektuDut(7);
			//harien errore bat saiezteko
			if(p==null || j==null)return null;
			pausoa paus = zerEginRiver(ciegaHandia, zerGertatuDa,j,p, potea, funtzioak.nirePosizioa);
			informazioaAzaleratu(ciegaHandia, zerGertatuDa, potea, 5);
			return xaguaMugitu(paus);

		}
		//jokoa hasi gabe dago
		else{
			System.err.println("[ERROREA]");
			return null;
		} 

	}

	private String xaguaMugitu(pausoa p) throws AWTException, InterruptedException{

		if(p.getZer().compareTo("fold")==0){
			xagua.foldEgin();
			return "fold";
		}
		else if(p.getZer().compareTo("call")==0){
			xagua.callEgin();
			return "call";
		}
		else if(p.getZer().compareTo("check")==0){
			xagua.checkEgin();
			return "check";
		}
		else if(p.getZer().compareTo("raise")==0){
			xagua.raiseEgin((int) p.getZenbat());
			return "raise";
		}
		else return null;

	}

	private pausoa zerEginPreFlop(int ciegaHandia, LinkedList<pausoa> zerGertatuDa, int potea, String pos) throws InterruptedException{

		int k=2;
		//karta guztiak lista batean laga
		LinkedList<karta> kartak = new LinkedList<karta>();	
		//oraindik eguneratu gabe
		while(k!=kartak.size()){
			System.out.println("[karten zain]["+k+"]"+"["+kartak.size()+"]");
			Thread.sleep(250);
			kartak = new LinkedList<karta>();
			if(funtzioak.nireKartak.size()>0)
			{
				kartak.add(funtzioak.nireKartak.getFirst());
				kartak.add(funtzioak.nireKartak.getLast());
			}
		}
		preFlop t = new preFlop();
		pausoa pausoa = t.zerEgin(ciegaHandia, zerGertatuDa, potea, kartak, pos);
		return pausoa;		
	}

	private pausoa zerEginFlop(int ciegaHandia, LinkedList<pausoa> zerGertatuDa, jugada j, LinkedList<jugada> p, int potea, String pos) throws InterruptedException{

		flop t = new flop();
		pausoa pausoa = t.zerEgin(ciegaHandia, zerGertatuDa, p,j, potea, funtzioak.nireKartak, pos);
		return pausoa;		
	}
	
	private pausoa zerEginTurn(int ciegaHandia, LinkedList<pausoa> zerGertatuDa, jugada j, LinkedList<jugada> p, int potea, String pos) throws InterruptedException{

		flop t = new flop();
		pausoa pausoa = t.zerEgin(ciegaHandia, zerGertatuDa, p,j, potea, funtzioak.nireKartak, pos);
		return pausoa;		
	}
	
	private pausoa zerEginRiver(int ciegaHandia, LinkedList<pausoa> zerGertatuDa, jugada j, LinkedList<jugada> p, int potea, String pos) throws InterruptedException{

		flop t = new flop();
		pausoa pausoa = t.zerEgin(ciegaHandia, zerGertatuDa, p,j, potea, funtzioak.nireKartak, pos);
		return pausoa;			
	}
	
	private jugada zeinJugadaDut(int k) throws InterruptedException{

		//karta guztiak lista batean laga
		LinkedList<karta> kartak = new LinkedList<karta>();	
		//oraindik eguneratu gabe
		while(k!=kartak.size()){

			System.out.println(funtzioak.zeMomentutan);

			//errore bat soluzionatzeko
			if(k>2 && funtzioak.zeMomentutan.compareTo("preFlop")==0)return null;

			System.out.println("[karten zain]["+k+"]"+"["+kartak.size()+"]");
			Thread.sleep(250);
			kartak = new LinkedList<karta>();
			if(funtzioak.nireKartak.size()>0)
			{
				kartak.add(funtzioak.nireKartak.getFirst());
				kartak.add(funtzioak.nireKartak.getLast());
				for(int i=0; i<funtzioak.erdikoKartak.size(); i++)
					kartak.add(funtzioak.erdikoKartak.get(i));
			}

		}	
		return handEvaluator.zeinJugadaDut(kartak);		
	}

	private LinkedList<jugada> zeinProiektuDut(int k) throws InterruptedException{

		//karta guztiak lista batean laga
		LinkedList<karta> kartak = new LinkedList<karta>();	
		//oraindik eguneratu gabe
		while(k!=kartak.size()){

			System.out.println(funtzioak.zeMomentutan);
			//errore bat soluzionatzeko
			if(k>2 && funtzioak.zeMomentutan.compareTo("preFlop")==0)return null;

			System.out.println("[karten zain]["+k+"]"+"["+kartak.size()+"]");
			Thread.sleep(250);
			kartak = new LinkedList<karta>();
			kartak.add(funtzioak.nireKartak.getFirst());
			kartak.add(funtzioak.nireKartak.getLast());
			for(int i=0; i<funtzioak.erdikoKartak.size(); i++)
				kartak.add(funtzioak.erdikoKartak.get(i));

		}	
		return handEvaluator.zeinProiektuDitut(kartak);		
	}


	private void informazioaAzaleratu(int ciegaHandia, LinkedList<pausoa> zerGertatuDa, int potea, int infor){

		System.out.println("===============================================");
		System.out.println("Potea = "+potea);
		//System.out.println("CiegaHAndia = "+ciegaHandia);
		System.out.println("Nire posizioa = "+funtzioak.nirePosizioa);
		System.out.println("Nire kartak = ["+funtzioak.nireKartak.getFirst().getNum()+" "+ikonoaBueltatu(funtzioak.nireKartak.getFirst().getPalo())+"] ["+funtzioak.nireKartak.getLast().getNum()+" "+ikonoaBueltatu(funtzioak.nireKartak.getLast().getPalo())+"]");
		System.out.print("Erdiko kartak = ");
		for(int i=0; i<infor;i++)System.out.print("["+funtzioak.erdikoKartak.get(i).getNum()+" "+ikonoaBueltatu(funtzioak.erdikoKartak.get(i).getPalo())+"] ");
		System.out.println();
		System.out.println("-------------------------------------------------");
		for(pausoa p : zerGertatuDa)
			System.out.println("["+p.getZeinek().getPosizioa()+"]  "+p.getZer()+" -> "+p.getZenbat());
		System.out.println("===============================================");

	}
	public String zeinPosiziotanNago(LinkedList<pausoa> zerGertatuDa){
		//6 jokalari daudenean bakarrik balio du
		int posizioa = 0;
		int utg = 0;

		//probatzeko
//		System.out.println("-------------------------------------");
//		for(int i=1; i<6; i++)System.out.println("Player"+i+" = "+funtzioak.zeinAusente[i]);
		
		
		//itxaron zer gertatu den jakin arte
		while(zerGertatuDa.size()==0){System.out.println("zerGertatuDa0000");}/** Gauza arraroak aurkitzen dira "zerGertatuDa"-n*/
		
		//UTG-a billatu
		for(int i=0; i<zerGertatuDa.size(); i++){

			System.out.println(zerGertatuDa.get(i).getZeinek().getPosizioa()+" ::: "+zerGertatuDa.get(i).getZer());
			if(zerGertatuDa.get(i).getZeinek().getPosizioa().compareTo("-1")!=0 && zerGertatuDa.get(i).getZer().compareTo("NULL")!=0)
			{
				utg = Integer.parseInt(zerGertatuDa.get(i).getZeinek().getPosizioa());
				break;
			}
		}
		
		System.out.println("UTG="+utg);
		System.out.println("-------------------------------------");
		//zenbat daue ausente
		int kop = 0;
		for(int i=1; i<utg; i++)//nire atzetik dauden ausente kopurua begiratu
			if(funtzioak.zeinAusente[i]==false)
				kop++;

		//posizioa kalkulatu, ausenteen arabera
		if(kop==0)
			posizioa = 6-utg+kop;

		if(posizioa==6 || posizioa==0)return "UTG";
		else if(posizioa==1)return "MP1";
		else if(posizioa==2)return "CO";
		else if(posizioa==3)return "BU";
		else if(posizioa==4)return "SB";
		else if(posizioa==5)return "BB";

		//zerbait gaizki joan da
		return null;
	}

	private String ikonoaBueltatu(String palo){

		if(palo.compareTo("d")==0)
			return "♦";
		else if(palo.compareTo("h")==0)
			return "♥";
		else if(palo.compareTo("s")==0)
			return "♠";
		else if(palo.compareTo("c")==0)
			return "♣";
		else System.err.println("[ERROREA]");
		return "Errorea";
	}

}
