package pokerOsoa;

import java.util.LinkedList;

import objetuak.karta;
import objetuak.pausoa;
import objetuak.player;

public class river {

	public pausoa zerEgin(int ciegaHandia, LinkedList<pausoa> zerGertatuDa, int potea, LinkedList<karta> kartak, String pos){


		if(funtzioak.zeMomentutan.compareTo("preFlop")==0){
			return preFlopp(ciegaHandia, zerGertatuDa, potea, kartak,pos);
		}
		else if(funtzioak.zeMomentutan.compareTo("flop")==0){
			return flop(ciegaHandia, zerGertatuDa, potea, kartak);
		}
		else if(funtzioak.zeMomentutan.compareTo("turn")==0){
			return turn(ciegaHandia, zerGertatuDa, potea, kartak);
		}
		else if(funtzioak.zeMomentutan.compareTo("river")==0){
			return river(ciegaHandia, zerGertatuDa, potea, kartak);
		}
		return null;
	}

	private pausoa preFlopp(int ciegaHandia, LinkedList<pausoa> zerGertatuDa, int potea, LinkedList<karta> kartak, String pos){

		//bi kartak ez begiratzeko
		handEvaluator.selectionSort(kartak);
		//gertatutakoa analizatu

		boolean limper = false;//funtzioa egin

		////////////////////7

		//return 4bb
	//	if(pos.compareTo("MP2")==0){

			//return 4bb

			if(!limper)
			{//norbaitek call egin ciega

				//pareja
				if(kartak.get(0).getNum().compareTo(kartak.get(1).getNum())==0){
					return new pausoa(new player(true, "0", -1), "raise", 3);
				}


				//A9o+
				else if(kartak.get(0).getNum().compareTo("J")==0 && kartak.get(1).getNum().compareTo("A")==0){
					return new pausoa(new player(true, "0", -1), "raise", 3);
				}
				else if(kartak.get(0).getNum().compareTo("Q")==0 && kartak.get(1).getNum().compareTo("A")==0){
					return new pausoa(new player(true, "0", -1), "raise", 3);
				}
				else if(kartak.get(0).getNum().compareTo("K")==0 && kartak.get(1).getNum().compareTo("A")==0){
					return new pausoa(new player(true, "0", -1), "raise", 3);
				}

				//figuras
				else if(kartak.get(0).getNum().compareTo("J")==0 && kartak.get(1).getNum().compareTo("A")==0){
					return new pausoa(new player(true, "0", -1), "raise", 3);
				}


				else if(kartak.get(0).getNum().compareTo("J")==0 || kartak.get(0).getNum().compareTo("Q")==0 || kartak.get(0).getNum().compareTo("K")==0){
					if(kartak.get(1).getNum().compareTo("J")==0 || kartak.get(1).getNum().compareTo("Q")==0 || kartak.get(1).getNum().compareTo("K")==0)
						return new pausoa(new player(true, "0", -1), "raise", 3);
				}

				//89s+
				else if(kartak.get(0).getPalo().compareTo(kartak.get(1).getPalo())==0){
					if(kartak.get(0).getNum().compareTo("9")==0 || kartak.get(0).getNum().compareTo("10")==0 || kartak.get(0).getNum().compareTo("J")==0 || kartak.get(0).getNum().compareTo("Q")==0 || kartak.get(0).getNum().compareTo("K")==0){
						if(kartak.get(1).getNum().compareTo("9")==0 || kartak.get(1).getNum().compareTo("10")==0 || kartak.get(1).getNum().compareTo("J")==0 || kartak.get(1).getNum().compareTo("Q")==0 || kartak.get(1).getNum().compareTo("K")==0)							
							return new pausoa(new player(true, "0", -1), "raise", 3);
					}

				}

				//Axs+
				else if(kartak.get(0).getPalo().compareTo(kartak.get(1).getPalo())==0){
					if(kartak.get(1).getNum().compareTo("A")==0){
						return new pausoa(new player(true, "0", -1), "raise", 3);
					}

				}
				return new pausoa(new player(true, "0", -1), "fold", -1);

			}
			else
			{//norbaitek caal egin ciega
				System.out.println();
			}


//		}
//		else if(pos.compareTo("CO")==0){
//
//		}
//		else if(pos.compareTo("BU")==0){
//
//		}
//		else if(pos.compareTo("SB")==0){
//
//		}
//		else if(pos.compareTo("BB")==0){
//
//		}
		return new pausoa(new player(true, "0", -1), "call", -1);
	}
	private pausoa flop(int ciegaHandia, LinkedList<pausoa> zerGertatuDa, int potea, LinkedList<karta> kartak){

		return null;
	}
	private pausoa turn(int ciegaHandia, LinkedList<pausoa> zerGertatuDa, int potea, LinkedList<karta> kartak){

		return null;
	}
	private pausoa river(int ciegaHandia, LinkedList<pausoa> zerGertatuDa, int potea, LinkedList<karta> kartak){

		return null;
	}

}
