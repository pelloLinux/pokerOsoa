package pokerOsoa;

import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.AWTException;

public class xaguaMugitu {



	public void raiseEgin(int zenbat) throws AWTException, InterruptedException{

		//-1 -> 3xCG edo 1/2
		//-2 -> Bote
		//-3 -> Max


		int time = hauzaskoZenbakiaItzuli(0, 3000);
		System.out.println("Erantzute denbota = "+time+" ms");
		Thread.sleep(time);

		if(zenbat == -1){

			if(funtzioak.zeMomentutan.compareTo("preFlop")==0)
				System.out.println("[RAISE 3xCG]");
			else 
				System.out.println("[RAISE 1/2]");


			Robot robot = new Robot();
			int xCoord = hauzaskoZenbakiaItzuli(1202, 1268);
			int yCoord = hauzaskoZenbakiaItzuli(740, 758);
			robot.mouseMove(xCoord, yCoord);
			robot.mousePress(InputEvent.BUTTON1_MASK);
			time = hauzaskoZenbakiaItzuli(10, 100);
			Thread.sleep(time);
			robot.mouseRelease(InputEvent.BUTTON1_MASK);


		}
		else if(zenbat == -2){

			System.out.println("[RAISE Bote]");

			Robot robot = new Robot();
			int xCoord = hauzaskoZenbakiaItzuli(1286, 1351);
			int yCoord = hauzaskoZenbakiaItzuli(740, 758);
			robot.mouseMove(xCoord, yCoord);
			robot.mousePress(InputEvent.BUTTON1_MASK);
			time = hauzaskoZenbakiaItzuli(10, 100);
			Thread.sleep(time);
			robot.mouseRelease(InputEvent.BUTTON1_MASK);


		}
		else if(zenbat == -3){

			System.out.println("[RAISE Max]");

			Robot robot = new Robot();
			int xCoord = hauzaskoZenbakiaItzuli(1373, 1435);
			int yCoord = hauzaskoZenbakiaItzuli(740, 758);
			robot.mouseMove(xCoord, yCoord);
			robot.mousePress(InputEvent.BUTTON1_MASK);
			time = hauzaskoZenbakiaItzuli(10, 100);
			Thread.sleep(time);
			robot.mouseRelease(InputEvent.BUTTON1_MASK);


		}
		else{

			System.out.println("[RAISE "+zenbat+"bb]");
			Robot robot = new Robot();
			int xCoord = hauzaskoZenbakiaItzuli(1400, 1433);
			int yCoord = hauzaskoZenbakiaItzuli(780, 792);
			
			//igoera zehatzenbat egin behar bada
			for(int i=0; i<zenbat; i++)
			{	
				robot.mouseMove(xCoord, yCoord);
				robot.mousePress(InputEvent.BUTTON1_MASK);
				time = hauzaskoZenbakiaItzuli(10, 100);
				Thread.sleep(time);
				robot.mouseRelease(InputEvent.BUTTON1_MASK);
				
				time = hauzaskoZenbakiaItzuli(200, 500);
				Thread.sleep(time);
			}
		}

		time = hauzaskoZenbakiaItzuli(1000, 3000);
		System.out.println("Erantzute denbota = "+time+" ms");
		Thread.sleep(time);


		Robot robot = new Robot();
		int xCoord = hauzaskoZenbakiaItzuli(1297, 1432);
		int yCoord = hauzaskoZenbakiaItzuli(820, 861);
		robot.mouseMove(xCoord, yCoord);
		robot.mousePress(InputEvent.BUTTON1_MASK);
		time = hauzaskoZenbakiaItzuli(10, 100);
		Thread.sleep(time);
		robot.mouseRelease(InputEvent.BUTTON1_MASK);
	}

	public void foldEgin() throws AWTException, InterruptedException{

		System.out.println("[FOLD]");

		int time = hauzaskoZenbakiaItzuli(0, 3000);
		System.out.println("Erantzute denbota = "+time+" ms");
		Thread.sleep(time);

		Robot robot = new Robot();
		int xCoord = hauzaskoZenbakiaItzuli(950, 1089);
		int yCoord = hauzaskoZenbakiaItzuli(820, 861);
		robot.mouseMove(xCoord, yCoord);
		robot.mousePress(InputEvent.BUTTON1_MASK);
		time = hauzaskoZenbakiaItzuli(10, 100);
		Thread.sleep(time);
		robot.mouseRelease(InputEvent.BUTTON1_MASK);

	}

	public void checkEgin() throws AWTException, InterruptedException{

		System.out.println("[CHECK]");

		int time = hauzaskoZenbakiaItzuli(0, 3000);
		System.out.println("Erantzute denbota = "+time+" ms");
		Thread.sleep(time);

		Robot robot = new Robot();
		int xCoord = hauzaskoZenbakiaItzuli(1124, 1265);
		int yCoord = hauzaskoZenbakiaItzuli(820, 861);
		robot.mouseMove(xCoord, yCoord);
		robot.mousePress(InputEvent.BUTTON1_MASK);
		time = hauzaskoZenbakiaItzuli(10, 100);
		Thread.sleep(time);
		robot.mouseRelease(InputEvent.BUTTON1_MASK);

	}

	public void callEgin() throws AWTException, InterruptedException{

		System.out.println("[CALL]");

		int time = hauzaskoZenbakiaItzuli(0, 3000);
		System.out.println("Erantzute denbota = "+time+" ms");
		Thread.sleep(time);

		Robot robot = new Robot();
		int xCoord = hauzaskoZenbakiaItzuli(1124, 1265);
		int yCoord = hauzaskoZenbakiaItzuli(820, 861);
		robot.mouseMove(xCoord, yCoord);
		robot.mousePress(InputEvent.BUTTON1_MASK);
		time = hauzaskoZenbakiaItzuli(10, 100);
		Thread.sleep(time);
		robot.mouseRelease(InputEvent.BUTTON1_MASK);			
	}

	private int hauzaskoZenbakiaItzuli(int min, int max)
	{
		numeroAleatorio randon = new numeroAleatorio(min, max);
		return randon.getRandom();
	}

}
