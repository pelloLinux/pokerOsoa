
Options: -D -Q weka.classifiers.bayes.net.search.local.K2 -- -P 1 -S BAYES -E weka.classifiers.bayes.net.estimate.SimpleEstimator -- -A 0.5 

Bayes Network Classifier
not using ADTree
#attributes=161 #classindex=160
Network structure (nodes followed by parents)
P_1(1): class 
P_2(1): class 
P_3(1): class 
P_4(2): class 
P_5(2): class 
P_6(2): class 
P_7(2): class 
P_8(1): class 
P_9(1): class 
P_10(2): class 
P_11(1): class 
P_12(1): class 
P_13(1): class 
P_14(3): class 
P_15(2): class 
P_16(2): class 
P_17(2): class 
P_18(1): class 
P_19(1): class 
P_20(2): class 
P_21(2): class 
P_22(1): class 
P_23(2): class 
P_24(2): class 
P_25(1): class 
P_26(1): class 
P_27(5): class 
P_28(2): class 
P_29(2): class 
P_30(2): class 
P_31(1): class 
P_32(2): class 
P_33(2): class 
P_34(6): class 
P_35(2): class 
P_36(2): class 
P_37(2): class 
P_38(5): class 
P_39(2): class 
P_40(1): class 
P_41(1): class 
P_42(2): class 
P_43(2): class 
P_44(2): class 
P_45(3): class 
P_46(3): class 
P_47(2): class 
P_48(2): class 
P_49(2): class 
P_50(6): class 
P_51(2): class 
P_52(5): class 
P_53(2): class 
P_54(2): class 
P_55(5): class 
P_56(1): class 
P_57(2): class 
P_58(2): class 
P_59(2): class 
P_60(3): class 
P_61(1): class 
P_62(1): class 
P_63(2): class 
P_64(2): class 
P_65(2): class 
P_66(2): class 
P_67(2): class 
P_68(2): class 
P_69(2): class 
P_70(1): class 
P_71(2): class 
P_72(2): class 
P_73(2): class 
P_74(2): class 
P_75(2): class 
P_76(2): class 
P_77(2): class 
P_78(2): class 
P_79(2): class 
P_80(1): class 
P_81(5): class 
P_82(2): class 
P_83(2): class 
P_84(2): class 
P_85(2): class 
P_86(2): class 
P_87(2): class 
P_88(2): class 
P_89(2): class 
P_90(2): class 
P_91(2): class 
P_92(4): class 
P_93(2): class 
P_94(2): class 
P_95(2): class 
P_96(2): class 
P_97(2): class 
P_98(2): class 
P_99(2): class 
P_100(2): class 
P_101(1): class 
P_102(5): class 
P_103(2): class 
P_104(2): class 
P_105(2): class 
P_106(2): class 
P_107(2): class 
P_108(2): class 
P_109(2): class 
P_110(2): class 
P_111(1): class 
P_112(5): class 
P_113(2): class 
P_114(2): class 
P_115(1): class 
P_116(1): class 
P_117(2): class 
P_118(2): class 
P_119(2): class 
P_120(2): class 
P_121(1): class 
P_122(2): class 
P_123(2): class 
P_124(2): class 
P_125(2): class 
P_126(2): class 
P_127(2): class 
P_128(2): class 
P_129(2): class 
P_130(1): class 
P_131(1): class 
P_132(5): class 
P_133(2): class 
P_134(5): class 
P_135(1): class 
P_136(1): class 
P_137(2): class 
P_138(2): class 
P_139(2): class 
P_140(1): class 
P_141(1): class 
P_142(1): class 
P_143(1): class 
P_144(5): class 
P_145(2): class 
P_146(2): class 
P_147(2): class 
P_148(1): class 
P_149(1): class 
P_150(1): class 
P_151(1): class 
P_152(2): class 
P_153(2): class 
P_154(1): class 
P_155(2): class 
P_156(1): class 
P_157(1): class 
P_158(1): class 
P_159(1): class 
P_160(1): class 
class(10): 
LogScore Bayes: -969.7467636982066
LogScore BDeu: -5706.2725265630925
LogScore MDL: -3782.366387267516
LogScore ENTROPY: -1929.936679953812
LogScore AIC: -3538.9366799538134


Time taken to build model: 0.05 seconds
Time taken to test model on training data: 0.01 seconds

=== Error on training data ===

Correctly Classified Instances          10              100      %
Incorrectly Classified Instances         0                0      %
Kappa statistic                          1     
Mean absolute error                      0     
Root mean squared error                  0     
Relative absolute error                  0      %
Root relative squared error              0      %
Total Number of Instances               10     


=== Confusion Matrix ===

 a b c d e f g h i j   <-- classified as
 1 0 0 0 0 0 0 0 0 0 | a = 1
 0 1 0 0 0 0 0 0 0 0 | b = 2
 0 0 1 0 0 0 0 0 0 0 | c = 3
 0 0 0 1 0 0 0 0 0 0 | d = 4
 0 0 0 0 1 0 0 0 0 0 | e = 5
 0 0 0 0 0 1 0 0 0 0 | f = 6
 0 0 0 0 0 0 1 0 0 0 | g = 7
 0 0 0 0 0 0 0 1 0 0 | h = 8
 0 0 0 0 0 0 0 0 1 0 | i = 9
 0 0 0 0 0 0 0 0 0 1 | j = 10


=== Error on test data ===

Correctly Classified Instances           1               10      %
Incorrectly Classified Instances         9               90      %
Kappa statistic                          0     
Mean absolute error                      0.18  
Root mean squared error                  0.4243
Relative absolute error                100      %
Root relative squared error            141.4214 %
Total Number of Instances               10     


=== Confusion Matrix ===

 a b c d e f g h i j   <-- classified as
 1 0 0 0 0 0 0 0 0 0 | a = 1
 1 0 0 0 0 0 0 0 0 0 | b = 2
 1 0 0 0 0 0 0 0 0 0 | c = 3
 1 0 0 0 0 0 0 0 0 0 | d = 4
 1 0 0 0 0 0 0 0 0 0 | e = 5
 1 0 0 0 0 0 0 0 0 0 | f = 6
 1 0 0 0 0 0 0 0 0 0 | g = 7
 1 0 0 0 0 0 0 0 0 0 | h = 8
 1 0 0 0 0 0 0 0 0 0 | i = 9
 1 0 0 0 0 0 0 0 0 0 | j = 10

